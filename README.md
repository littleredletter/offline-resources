## Offline Resources Repo

When working offline set a VHost for `ajax.googleapis.com` to point to this repo.

### Supported Resources

* jQuery
* jQuery UI
* Web Font Loader